<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homecontroller;
use App\Http\Controllers\authcontroller;
use App\Http\Controllers\castcontroller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [homecontroller:: class,'home']);

Route::get('/daftar', [authcontroller:: class,'daftar']);
Route::get('/kirim', [authcontroller:: class,'kirim']);

Route::get('/table', function(){
    return view('page.table');
});

Route::get('/data-table', function(){
    return view('page.datatable');
});

//CRUD

//Create data
//route untuk mengarah ke form tambah cast
Route::get('/cast/create',[castcontroller::class,'create']);
//Route untuk meyimoan data inputan ke data base table cast
Route::post('/cast',[castcontroller::class,'store']);

//read data
//route untuk menampilkan semua data yang ada di table cast data base
Route::get('/cast',[castcontroller::class,'index']);
//route ambil ditail data besrdasarkan id nya
Route::get('/cast/{id}',[castcontroller::class,'show']);

//update data
//route untuk mengarah ke form edit cast dengan membawa data berdasarkan id
Route::get('/cast/{id}/edit', [castcontroller::class,'edit']);
//route untuk update cast berdasarkan id
Route::put('/cast/{id}',[castcontroller::class,'update']);


//delate data
//route hapus data berdasarkan id cast
Route::delete('/cast/{id}',[castcontroller::class,'destroy']);