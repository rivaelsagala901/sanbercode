@extends('layout.master')
@section('title')
    Halaman tambah cast  
@endsection
@section('content')
<form method="post" action="/cast">
    @csrf
    <div class="form-group">
      <label>cast nama</label>
      <input type="text" name="nama" class="form-control" >
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>cast umur</label>
        <input type="text" name="umur" class="form-control" >
      </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label >cast bio</label>
      <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection