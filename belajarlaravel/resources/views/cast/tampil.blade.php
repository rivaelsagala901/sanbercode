@extends('layout.master')
@section('title')
    Halaman tampil cast  
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-2">tambah</a>

<table class="table table-dark">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">nama</th>
        <th scope="col">action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=> $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
            
        @empty
            <tr>
                <td>Data cast kosong</td>
            </tr>
        @endforelse

    </tbody>
  </table>  

@endsection