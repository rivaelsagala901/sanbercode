@extends('layout.master')
@section('title')
    Halaman tampil edit cast  
@endsection
@section('content')

@extends('layout.master')
@section('title')
    Halaman tambah cast  
@endsection
@section('content')
<form method="post" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>cast nama</label>
      <input type="text" value="{{$cast->nama}}" name="nama" class="form-control" >
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>cast umur</label>
        <input type="text" value="{{$cast->umur}}" name="umur" class="form-control" >
      </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label >cast bio</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection

@endsection