<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>register</title>
</head>
<body>
    <h1>Buat Akun Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim">
        @csrf
        <label>First name:</label><br><br>   
        <input type="text"><br><br>
        <label>Last name:</label><br><br>
        <input type="text"> <br><br>
        
        <label>Gender:</label><br><br>
        <input type="radio" name="status">
        <label>Male</label><br>
        <input type="radio" name="status">
        <label>Female</label><br>
        <input type="radio" name="status">
        <label>Other</label><br><br>

        <label>Nationality:</label><br><br>
        <select name="" id="">
            <option value="">Indonesian</option>
            <option value="">Brazil</option>
            <option value="">Portugal</option>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="" id="">
        <label>Bahasa Indonesia</label><br>
        <input type="checkbox" name="" id="">
        <label>English</label><br>
        <input type="checkbox" name="" id="">
        <label>Other</label><br><br>

        <label>Bio:</label><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br><br>

        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTMyU28V_g7wDYF3D0LJdZtQCo80jEXrGX0Lw&usqp=CAU" alt="">
        <p>Enter <a href="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTMyU28V_g7wDYF3D0LJdZtQCo80jEXrGX0Lw&usqp=CAU">Here</a> for This image</p><br><br><br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>